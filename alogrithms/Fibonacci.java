import java.util.ArrayList;

public class Fibonacci {

    /**
     * Determines if string is palindrome by iterating by count of length string and references the char at the index of the
     * iterating cursor, one moving forward and the other in reverse
     * This presents a BigO complexity of O(2^N) -- not an ideally efficient method, but can be solved via memiozation to store repeated
     * calculated results in an array and act as a cache reference for repeated calculation operations
     * @param int
     * @return fibonacci value at incoming index
     */
    private static int fibonacci(int n) {   
        // System.out.println("N!!!!: " + n);    
        if (n <= 1) {
            return n;
        }
        
        int[] fibArr = new int[n + 2]; //To store results as caching dynamic programming approach to avoid repeated work and to scale

        for (int i = 2; i <= n; i++) {
            fibArr[i] = fibonacci(n - 1) + fibonacci(n - 2);
        }

        // System.out.println("fibonacci(n - 1): "+ fibonacci(n - 1));
        // System.out.println("fibonacci(n - 2): "+ fibonacci(n - 2));
        return fibArr[n];
        // return n;
    }

    public static void main(String[] args) { 
        int fibN = 7;
        long startTime = System.nanoTime();
        System.out.println(fibonacci(fibN));
        long endTime = System.nanoTime();
        System.out.println("Execution time: " + (endTime - startTime) + " ms");
    }
}