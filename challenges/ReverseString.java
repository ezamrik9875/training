
public class ReverseString {

    /**
     * Reverses the incoming string value through use of char array and for loop
     * This presents a BigO complexity of O(n) -- pretty efficient since complexity shows efficiency is linear based on the size 
     * of the incoming data
     * @param str
     * @return reversed string of incoming string
     */
    private static String reverseString(String str) {

        char[] strChar = str.toCharArray();
        char[] strRev = new char[str.length()];
        int i = strChar.length - 1;
        int j = 0;
        
        for (i = strChar.length - 1; i >= 0 ; i--) {
            strRev[j] = strChar[i];
            j++;
        }

        return String.valueOf(strRev);
    }

    /**
     * Reverses the incoming string value through use of char array and while loop
     * This presents a BigO complexity of O(n) -- pretty efficient since complexity shows efficiency is linear based on the size
     * of the incoming data. While loop very slightly more efficient than for loop, but too minimal to be significant when making optimization decision and due 
     * to sample size of testing and variance caused by factors that are uncontrollable and minimal differences, it would fair to say that a while loop and for loop
     * hava no performance difference at all
     * @param str
     * @return reversed string of incoming string
     */
    private static String reverseStringWhile(String str) {

        char[] strChar = str.toCharArray();
        char[] strRev = new char[str.length()];
        int i = strChar.length - 1;
        int j = 0;
        
        while(i >= 0) {
            strRev[j] = strChar[i];
            j++;
            i--;
        }

        return String.valueOf(strRev);
    }

    /**
     * Reverses the incoming string value through use of byte array and for loop
     * This presents a BigO complexity of O(n) -- pretty efficient since complexity shows efficiency is linear based on the size
     * of the incoming data.
     * @param str
     * @return reversed string of incoming string
     */
    private static String reverseStringByte(String str) {

        char[] strChar = str.toCharArray();
        char[] strRev = new char[str.length()];
        int i = strChar.length - 1;
        int j = 0;
        
        while(i >= 0) {
            strRev[j] = strChar[i];
            j++;
            i--;
        }

        return String.valueOf(strRev);
    }

    public static void main(String[] args) {
        long startTime = System.nanoTime();
        System.out.println(reverseString("abc"));
        long endTime = System.nanoTime();
        System.out.println("Execution time: " + (endTime - startTime) + " ms");

        //Keep note that when running functions in succession, the first function will always have a longer delay due to inital overhead initialization. 
        //Function performance should be determined by running each function independently per java program call or wrapped
        startTime = System.nanoTime();
        System.out.println(reverseStringWhile("abc"));
        endTime = System.nanoTime();
        System.out.println("Execution time: " + (endTime - startTime) + " ms");
    }
}