package arrays;

/**
 * Challenge: https://www.hackerrank.com/challenges/new-year-chaos/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=arrays&h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen
 */
public class NewYearChaos {

    private static String minimumBribes(int[] arr) {

        int totalBribes = 0;

        for (int i = 0; i < arr.length; i++) {

            //Determine if current val created chaos
            if(arr[i] - (i + 1) > 2) {
                return "Too chaotic";
            }

            //Determine if val made a bribe
            if(arr[i] > (i + 1)) {
                totalBribes += (arr[i] - (i + 1)); //calculate n bribe
            }

            // if(i < arr.length - 1 && arr[i] > arr[i + 1]) {
            //     totalBribes += (arr[i] - (i + 1));
            // }
        }

        return String.valueOf(totalBribes);
    }

    public static void main(String[] args) {

        //Non-chaotic
        int[] stableArr = {2, 1, 5, 3, 4};

        //Non-chaotic - tricky
        int[] stabArrTrick = {1, 2, 5, 3, 7, 8, 6, 4};

        //Too-chaotic
        int[] chaoticArr = {2, 5, 1, 3, 4};

        System.out.println("Result: " + minimumBribes(stableArr));

    }
}