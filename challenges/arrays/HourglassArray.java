package arrays;

/**
 * Challenge: https://www.hackerrank.com/challenges/2d-array/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=arrays
 */
public class HourglassArray {

    private static int maximumHourglass(int[][] arr) {
        int hourglassBase = 0;
        int hourGlassMid = 1;
        int sum = 0;
        int max = Integer.MIN_VALUE;

        for(int i = 0; i < arr.length; i++) {
            hourglassBase = 0;
            hourGlassMid = 1;
            // System.out.println("i: " + i);
            for (int j = 0; j < arr[i].length; j++)  {
                if(i + 2 < arr.length && j + 2 < arr[i].length) {
                    // System.out.println("i in: " + i);
                    // System.out.println("j: " + j);

                    sum = arr[i][hourglassBase] + 
                          arr[i][hourglassBase + 1] + 
                          arr[i][hourglassBase + 2] + 
                          arr[i + 1][hourGlassMid] + 
                          arr[i+2][hourglassBase] + 
                          arr[i+2][hourglassBase + 1] + 
                          arr[i+2][hourglassBase + 2];

                    // System.out.println(arr[i][hourglassBase] + " " + arr[i][hourglassBase + 1] + " " + arr[i][hourglassBase + 2]);
                    // System.out.println(arr[i + 1][hourGlassMid]);
                    // System.out.println( arr[i+2][hourglassBase] + " " + arr[i+2][hourglassBase + 1] + " " + arr[i+2][hourglassBase + 2]);
                    // System.out.println("Sum Calculated: " + sum);

                    if(sum > max) {
                        max = sum;
                        // System.out.println("New max: " + max);
                    }

                    hourglassBase++;
                    hourGlassMid++;
                }
            }
        }

        return max;
    }

    public static void main(String args[]) { 
        int[][] arr = {
            {1, 1, 1, 0, 0, 0},
            {0, 1, 0, 0, 0, 0},
            {1, 1, 1, 0, 0, 0},
            {0, 0, 2, 4, 4, 0},
            {0, 0, 0, 2, 0, 0},
            {0, 0, 1, 2, 4, 0}
        };

        int[][] arr2 = {
            {-9, -9, -9,  1, 1, 1 },
            { 0, -9,  0,  4, 3, 2},
            {-9, -9, -9,  1, 2, 3},
            { 0,  0,  8,  6, 6, 0},
            { 0,  0,  0, -2, 0, 0},
            { 0,  0,  1,  2, 4, 0} 
        };

        //Scenario - Highest value will be negative 
        int[][] arr3 = {
            {-1, -1,  0, -9, -2, -2},
            {-2, -1, -6, -8, -2, -5},
            {-1, -1, -1, -2, -3, -4},
            {-1, -9, -2, -4, -4, -5},
            {-7, -3, -3, -2, -9, -9},
            {-1, -3, -1, -2, -4, -5} 
        };

        System.out.println("The greatest hourglass sum is: " + maximumHourglass(arr3));
    }
}