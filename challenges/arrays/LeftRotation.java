package arrays;

/**
 * Challenge: https://www.hackerrank.com/challenges/ctci-array-left-rotation/problem?h_l=interview&playlist_slugs%5B%5D%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D%5B%5D=arrays
 */
public class LeftRotation {

    /**
     * Rotates the int array from the left based on the number of rotations defined
     * Uses a for loop to control the number of rotations and an inner for loop to shift 
     * numbers to the left within the array
     * This presents a BigO complexity of *Not determined*
     * *Key takeaway* - Used the array itself to create the shift, creating a clean minimal code solution 
     * *Potential other solution* - Might be dirtier/more complicated, try moving the values to another array
     * @param int[] array of integers
     * @param int number of rotations
     * @return array rotated from the left based number of rotations inputted
     */
    private static int[] rotateLeft(int[] arr, int rotations) {
        int val = 0;

        for(int i = 0; i < rotations; i++) {
            val = arr[0];
            for(int j = 0; j < arr.length - 1; j++) {
                arr[j] = arr[j + 1];
            }
            arr[arr.length - 1] = val;
        }

        return arr;
    }

    public static void main(String[] args) {

        int[] arr = {1, 2, 3, 4, 5};
        int rot = 4;

        System.out.println("Left Rotated Array: ");
        int[] rotatedArr = rotateLeft(arr, rot);
        for(int i = 0; i < rotatedArr.length; i++) { 
            System.out.print(rotatedArr[i] + " ");
        }
    }
}