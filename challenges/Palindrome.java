
public class Palindrome {

    /**
     * Determines if string is palindrome using two for loops, one going forward and the other going backwards
     * This presents a BigO complexity of O(n^2) -- there is likely a more efficient way
     * @param str
     * @return true or false
     */
    private static boolean isPalindrome(String str) {
        
        char[] palChar = str.toCharArray();

        for (int i = 0; i < palChar.length - 1; i++) {
            for (int v = (palChar.length - 1) - i; v >= 0; v--) { 
                if(palChar[v] == palChar[i]) {
                    break;
                }

                return false;
            }
        }

        return true;
    }

    /**
     * Determines if string is palindrome by iterating by count of length string and references the char at the index of the
     * iterating cursor, one moving forward and the other in reverse
     * This presents a BigO complexity of O(n) -- a pretty efficient version of the method since it's efficiency is purely linear based on incoming data
     * @param str
     * @return true or false
     */
    private static boolean isPalindrome2(String str) {
        
        int i = 0;
        int j = str.length() - 1;

        while (i < j) { 

            if(str.charAt(i) != str.charAt(j)) {
                return false;
            }

            i++;
            j--;
        }

        return true;
    }



    public static void main(String[] args) {
        long startTime = System.nanoTime();
        System.out.println(isPalindrome("MADAM"));
        long endTime = System.nanoTime();
        System.out.println("Execution time: " + (endTime - startTime) + " ms");

        startTime = System.nanoTime();
        System.out.println(isPalindrome2("MADAM"));
        endTime = System.nanoTime();
        System.out.println("Execution time 2: " + (endTime - startTime) + " ms");
    }
}